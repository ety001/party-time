-- phpMyAdmin SQL Dump
-- version 3.5.3
-- http://www.phpmyadmin.net
--
-- 主机: 10.4.3.92
-- 生成日期: 2013 年 12 月 29 日 19:00
-- 服务器版本: 5.1.72-0ubuntu0.10.04.1
-- PHP 版本: 5.3.2-1ubuntu4.19

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `d8d9c014140084e4986617ff865624a08`
--

-- --------------------------------------------------------

--
-- 表的结构 `p_party`
--

CREATE TABLE IF NOT EXISTS `p_party` (
  `party_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'party_id',
  `user_id` int(11) NOT NULL COMMENT 'user_id',
  PRIMARY KEY (`party_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `p_timetable`
--

CREATE TABLE IF NOT EXISTS `p_timetable` (
  `timetable_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'time_id',
  `party_id` int(11) NOT NULL COMMENT 'party_id',
  `date` int(11) NOT NULL COMMENT '日期',
  `time_ period` int(11) NOT NULL DEFAULT '0' COMMENT '0全天,1上午,2下午,3晚上',
  PRIMARY KEY (`timetable_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `p_user`
--

CREATE TABLE IF NOT EXISTS `p_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `realname` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(32) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `p_user_select`
--

CREATE TABLE IF NOT EXISTS `p_user_select` (
  `select_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `party_id` int(11) NOT NULL,
  `timetable_id` int(11) NOT NULL,
  PRIMARY KEY (`select_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 2013-12-29 20：33
ALTER TABLE  `p_party` ADD  `title` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER  `party_id`;

-- 2013-12-30 21:34
--
-- 表的结构 `p_acl`
--

CREATE TABLE IF NOT EXISTS `p_acl` (
  `aclid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `controller` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `acl_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`aclid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='权限表';

-- 2013-12-30 23：26
ALTER TABLE  `p_party` ADD  `create_time` INT NOT NULL COMMENT  '创建时间' AFTER  `user_id`;

-- 2014-01-01 16:00
ALTER TABLE  `p_party` ADD  `deadline` INT NOT NULL COMMENT  'vote deadline' AFTER  `create_time`;

-- 2014-01-01 17:56
ALTER TABLE  `p_party` ADD  `start_date` INT NOT NULL COMMENT  'vote_period_start' AFTER  `user_id` ,
ADD  `end_date` INT NOT NULL COMMENT  'vote_period_end' AFTER  `start_date`;

-- 2014-01-01 18:00
ALTER TABLE  `p_party` ADD  `status` INT( 1 ) NOT NULL DEFAULT  '1' COMMENT  '1open,0close' AFTER  `user_id`;

-- 2014-01-02 23:36
DROP TABLE  `p_user_select`;

-- 2014-01-02 23:36
RENAME TABLE  `party`.`p_timetable` TO  `party`.`p_user_select` ;

-- 2014-01-02 23:37
ALTER TABLE  `p_user_select` CHANGE  `timetable_id`  `select_id` INT( 11 ) NOT NULL AUTO_INCREMENT COMMENT  'select_id';

-- 2014-01-02 23:38
ALTER TABLE  `p_user_select` ADD  `user_id` INT NOT NULL AFTER  `select_id`;

-- 2014-01-05 11:44
ALTER TABLE  `p_user_select` CHANGE  `time_ period`  `time_period` INT( 11 ) NOT NULL DEFAULT  '0' COMMENT  '0全天,1上午,2下午,3晚上';

-- 2014-01-05 18:02
ALTER TABLE  `p_user_select` ENGINE = INNODB;

-- 2014 01-05 23:56
INSERT INTO `p_acl` VALUES ('1', '所有聚会首页', 'party', 'index', 'USER');
INSERT INTO `p_acl` VALUES ('2', '保存聚会', 'party', 'saveParty', 'USER');
INSERT INTO `p_acl` VALUES ('3', '选择聚会时间', 'party', 'selectTime', 'USER');
INSERT INTO `p_acl` VALUES ('4', '保存聚会时间', 'party', 'saveSel', 'USER');




/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
