<?php /* Smarty version Smarty-3.0.8, created on 2013-12-02 05:05:58
         compiled from "/home/ety001/wwwroot/auto-trade-btc-robot/source/tpl/main/index.html" */ ?>
<?php /*%%SmartyHeaderCode:1030280751529c1536d4e3f7-48719827%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1527e139bbf747c1e834834ad58ce5848e4fe43f' => 
    array (
      0 => '/home/ety001/wwwroot/auto-trade-btc-robot/source/tpl/main/index.html',
      1 => 1385960747,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1030280751529c1536d4e3f7-48719827',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html>
<html>
  <head>
    <title>Auto Trade BTC</title>
    <script type="text/javascript">
        //api URI
        var apiURI = "<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['spUrl'][0][0]->__template_spUrl(array('c'=>'main','a'=>'api'),$_smarty_tpl);?>
";
        var tradeApiURI = "<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['spUrl'][0][0]->__template_spUrl(array('c'=>'main','a'=>'tradeInfo'),$_smarty_tpl);?>
";
    </script>
    <?php $_template = new Smarty_Internal_Template("header.html", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
  </head>

  <body>
    <!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-fixed-top top">
      <div class="container">
        <span>欢迎，<strong id="username"><?php echo $_smarty_tpl->getVariable('username')->value;?>
</strong></span>
        <span>可用余额：฿<strong id="btc_b"><?php echo $_smarty_tpl->getVariable('btc_b')->value;?>
</strong>&nbsp;&nbsp;¥<strong id="cny_b"><?php echo $_smarty_tpl->getVariable('cny_b')->value;?>
</strong></span>
        <span>冻结金额：฿<strong id="btc_f"><?php echo $_smarty_tpl->getVariable('btc_f')->value;?>
</strong>&nbsp;&nbsp;¥<strong id="cny_f"><?php echo $_smarty_tpl->getVariable('cny_f')->value;?>
</strong></span>
      </div>
      <div class="container">
        <span>最新成交价：<strong id="last"><?php echo $_smarty_tpl->getVariable('last')->value;?>
</strong></span>
        <span>买一：<strong id="buy"><?php echo $_smarty_tpl->getVariable('buy')->value;?>
</strong></span>
        <span>卖一：<strong id="sell"><?php echo $_smarty_tpl->getVariable('sell')->value;?>
</strong></span>
        <span>最高：<strong id="high"><?php echo $_smarty_tpl->getVariable('high')->value;?>
</strong></span>
        <span>最低：<strong id="low"><?php echo $_smarty_tpl->getVariable('low')->value;?>
</strong></span>
      </div>
    </div>

    <div class="container" style="margin-top:60px;">
      <div class="panel panel-info">
        <div class="panel-heading">Bids & Asks</div>
        <div class="panel-body">
          <!--buy-->
          <div class="panel panel-info buy_sell">
            <div class="panel-heading">BUY</div>
            <div class="panel-body">
              <form action="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['spUrl'][0][0]->__template_spUrl(array('c'=>'main','a'=>'buy'),$_smarty_tpl);?>
" method="post">
                <div class="input-group">
                  <input type="text" class="form-control" id="buy_amount" name="buy_amount" placeholder="Amount To Buy">
                  <span class="input-group-addon">BTC</span>
                </div>
                <div class="input-group">
                  <input type="text" class="form-control" id="buy_price" name="buy_price" placeholder="Price of Each">
                  <span class="input-group-addon">CNY</span>
                </div>
                <div class="input-group">
                  <input type="text" class="form-control" id="buy_order" placeholder="Order Value">
                  <span class="input-group-addon">CNY</span>
                </div>
                <div class="input-group">
                  <input type="submit" value="BUY">
                </div>
              </form>
            </div>
          </div>
          <!--sell-->
          <div class="panel panel-info buy_sell">
            <div class="panel-heading">SELL</div>
            <div class="panel-body">
              <form action="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['spUrl'][0][0]->__template_spUrl(array('c'=>'main','a'=>'sell'),$_smarty_tpl);?>
" method="post">
                <div class="input-group">
                  <input type="text" class="form-control" id="sell_amount" name="sell_amount" placeholder="Amount To Sell">
                  <span class="input-group-addon">BTC</span>
                </div>
                <div class="input-group">
                  <input type="text" class="form-control" id="sell_price" name="sell_price" placeholder="Price of Each">
                  <span class="input-group-addon">CNY</span>
                </div>
                <div class="input-group">
                  <input type="text" class="form-control" id="sell_order" placeholder="Order Value">
                  <span class="input-group-addon">CNY</span>
                </div>
                <div class="input-group">
                  <input type="submit" value="SELL">
                </div>
              </form>
            </div>
          </div>
          <!--BIDS-->
          <table class="table table-bordered table-striped bid_ask">
            <tr>
              <th>Amount</th>
              <th>Bids</th>
            </tr>
            <?php  $_smarty_tpl->tpl_vars['bid'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('bids')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['bid']->key => $_smarty_tpl->tpl_vars['bid']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['bid']->key;
?>
            <tr>
              <td><?php echo $_smarty_tpl->tpl_vars['bid']->value['amount'];?>
</td>
              <td><a href="javascript:Order.sell(<?php echo $_smarty_tpl->tpl_vars['bid']->value['price'];?>
)"><?php echo $_smarty_tpl->tpl_vars['bid']->value['price'];?>
</a></td>
            </tr>
            <?php }} ?>
          </table>
          <!--ASKS-->
          <table class="table table-bordered table-striped bid_ask">
            <tr>
              <th>Asks</th>
              <th>Amount</th>
            </tr>
            <?php  $_smarty_tpl->tpl_vars['ask'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('asks')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['ask']->key => $_smarty_tpl->tpl_vars['ask']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['ask']->key;
?>
            <tr>
              <td><a href="javascript:Order.buy(<?php echo $_smarty_tpl->tpl_vars['ask']->value['price'];?>
)"><?php echo $_smarty_tpl->tpl_vars['ask']->value['price'];?>
</a></td>
              <td><?php echo $_smarty_tpl->tpl_vars['ask']->value['amount'];?>
</td>
            </tr>
            <?php }} ?>
          </table>
        </div>
      </div>
    </div>
    <script src="./public/js/trade.js"></script>
  </body>
</html>