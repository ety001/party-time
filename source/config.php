<?php
define("APP_PATH",dirname(__FILE__));
define("SP_PATH",dirname(__FILE__).'/SpeedPHP');
$spConfig = array(
	'db' => array( // 数据库设置
	    'driver' => 'mysqli',
        'host' => getenv("MOPAAS_MYSQL5462_HOST"),  // 数据库地址
        'login' => getenv("MOPAAS_MYSQL5462_USER"), // 数据库用户名
        'password' => getenv("MOPAAS_MYSQL5462_PASSWORD"), // 数据库密码
        'database' => getenv("MOPAAS_MYSQL5462_NAME"), // 数据库的库名称
        'prefix' => getenv("P_PREFIX") // 表前缀
    ),
	'view' => array(
		'enabled' => TRUE, // 开启Smarty支持
		'config' =>array(
			'template_dir' => APP_PATH.'/tpl', // 模板页面所在的目录
			'compile_dir' => APP_PATH.'/tmp', // 临时文件编译目录
			'cache_dir' => APP_PATH.'/tmp', // 临时文件缓存目录
			'left_delimiter' => '<{',  // Smarty左限定符，默认是{
			'right_delimiter' => '}>', // Smarty右限定符，默认是}
		),
	),
    'launch' => array( // 加入挂靠点，以便开始使用Url_ReWrite的功能
        'router_prefilter' => array(
            array('spAcl','mincheck'), // 开启有限的权限控制
            array('spUrlRewrite', 'setReWrite'),  // 对路由进行挂靠，处理转向地址
        ),
         'function_url' => array(
            array("spUrlRewrite", "getReWrite"),  // 对spUrl进行挂靠，让spUrl可以进行Url_ReWrite地址的生成
        ),
    ),
    'ext' => array(
        'spUrlRewrite' => array(
            'suffix' => '', 
            'sep' => '/', 
            'map' => array(
                'closeOvertimeParty' => 'cron@closeOvertimeParty',
                'index' => 'main@index',
                'message' => 'msg@index',
                'party' => 'party@index',
                'qrcode' => 'party@partyQrCode',
                'saveParty' => 'party@saveParty',
                'selectTime' => 'party@selectTime',
                'saveSel' => 'party@saveSel',
                'show' => 'party@show',
                'user' => 'user@index',
                'register' => 'user@register',
                'addUser' => 'user@addSave',
                'signin' => 'user@signin',
                'signAuth' => 'user@signAuth',
                'signout' => 'user@signout',
                'showDetail' => 'party@showDetail'  
            ),
            'args' => array(
                'party' => array('type','page'),
                'qrcode' => array('pid','pix'),
                'selectTime' => array('pid'),
                'show' => array('pid'),
                'register' => array('lastPage'),
                'signin' => array('lastPage'),
                'showDetail' => array('party_id','date','time_period')
            )
        ),
    )
);