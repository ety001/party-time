<?php
class m_user_select extends spModel
{
    var $pk = "select_id"; // 数据表的主键
    var $table = "user_select"; // 数据表的名称
    public function addMore($data){
        global $spConfig;
        if(!$data)return;
        $sql = 'insert into '.$spConfig['db']['prefix'].$this->table.' (user_id,party_id,date,time_period) values ';
        $valArr = array();
        foreach ($data as $k => $v) {
            array_push($valArr,'('.$v['user_id'].','.$v['party_id'].','.$v['date'].','.$v['time_period'].')');
        }
        $val = implode(',', $valArr);
        $sql = $sql . $val;
        return $this->runSql($sql);
    }
    public function IattendedPartyIDs(){
        $conditions = array('user_id'=>$_SESSION['userInfo']['user_id']);
        $partyIDArr = $this->findAll($conditions,null,'party_id');
        $partyIDS = array();
        foreach ($partyIDArr as $k => $v) {
            if(!in_array($v['party_id'], $partyIDS)){
                array_push($partyIDS, $v['party_id']);
            }
        }
        $ids = implode(',', $partyIDS);
        return $ids;
    }
}