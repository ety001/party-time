<?php
class m_user extends spModel
{
    var $pk = "user_id"; // 数据表的主键
    var $table = "user"; // 数据表的名称

    var $verifierReg = array(
        "rules" => array( // 规则
            'realname' => array(  // 这里是对realname的验证规则
                'notnull' => TRUE, // realname不能为空
                'minlength' => 2,  // realname长度不能小于2
                'maxlength' => 50, // realname长度不能大于50
            ),
            'email' => array(   // 这里是对email的验证规则
                'notnull' => TRUE, // email不能为空
                'isEmailExist' => TRUE, //email不能存在
                'email' => TRUE,   // 必须要是电子邮件格式
                'minlength' => 6,  // email长度不能小于6
                'maxlength' => 100, // email长度不能大于20
            ),
            'password' => array(
                'minlength' => 8,
                'notnull' => TRUE,
            ),
            'repass' => array(
                'equalto' => 'password'
            )
        ),
        "messages" => array( // 提示信息
            'realname' => array(
                'notnull' => '真实名字不能为空',
                'minlength' => '真实名字不能少于2个字符',
                'maxlength' => '真实名字不能大于50个字符',
            ),
            'email' => array(
                'notnull' => '邮箱地址不能空',
                'isEmailExist' => '输入的邮箱地址已经注册',
                'email' => '请输入一个正确的邮箱地址',
                'minlength' => '邮箱地址的长度不能小于6个字符',
                'maxlength' => '邮箱地址的长度不能大于100个字符'
            ),
            'password' => array(
                'minlength' => '密码的长度不能小于8个字符',
                'notnull' => '密码不能为空'
            ),
            'repass' => array(
                'equalto' => '两次密码不同'
            )
        )
    );

    var $verifierSignin = array(
        "rules" => array( // 规则
            'email' => array(   // 这里是对email的验证规则
                'notnull' => TRUE, // email不能为空
                'email' => TRUE,   // 必须要是电子邮件格式
                'minlength' => 6,  // email长度不能小于6
                'maxlength' => 100, // email长度不能大于20
            ),
            'password' => array(
                'notnull' => TRUE,
            )
        ),
        "messages" => array( // 提示信息
            'email' => array(
                'notnull' => '邮箱地址不能空',
                'email' => '请输入一个正确的邮箱地址',
                'minlength' => '邮箱地址的长度不能小于6个字符',
                'maxlength' => '邮箱地址的长度不能大于100个字符'
            ),
            'password' => array(
                'notnull' => '密码不能为空'
            )
        )
    );

    var $addrules = array(
        'isEmailExist' => array('m_user','checkEmailExist')
    );

    public function checkEmailExist($input, $verifiering){
        $result = $this->findBy('email',$input);
        if($verifiering == $result){
            return FALSE;
        } else {
            return TRUE;
        }
    }
}