<?php
class authParent extends spController
{
    // 构造函数，进行全局操作的位置
    function __construct(){
        // 必须加入启动父类构造函数的操作
        parent::__construct();
        
        // 开始全局操作
        //is login or not
        if(isset($_SESSION['userInfo'])){
            $this->isLogin = 1;
        } else {
            $this->isLogin = 0;
        }

        //week array
        $this->week = array(
            0=>'日',
            1=>'一',
            2=>'二',
            3=>'三',
            4=>'四',
            5=>'五',
            6=>'六',
        );

        //is debug
        global $spConfig;
        if($spConfig['mode'] == 'debug'){
            $this->isDebug = 1;
        } else {
            $this->isDebug = 0;
        }
        
    }
}