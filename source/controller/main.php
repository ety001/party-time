<?php
class main extends authParent
{
    public function index(){
        if($this->isLogin){
            $this->jump(spUrl('party','index'));
            return;
        }
        $this->display('main/index.html');
    }
}