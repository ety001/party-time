<?php
class party extends authParent
{
    public function index(){
        $type = ($this->spArgs('type')=='attend')?'attend':'create';
        $page = (int)$this->spArgs('page',1);
        $partyObj = spClass('m_party');
        $confident = array('user_id'=>$_SESSION['userInfo']['user_id']);
        switch ($type) {
            case 'create':
                $results = $partyObj->spPager($page , 10)->findAll($confident,'party_id desc'); 
                $pager = $partyObj->spPager()->getPager();
                break;
            case 'attend':
                $userSelObj = spClass('m_user_select');
                $ids = $userSelObj->IattendedPartyIDs();
                if(!$ids)break;
                $conditionStr = ' party_id in ('.$ids.') ';
                $results = $partyObj->spPager($page , 10)->findAll($conditionStr,'party_id desc');
                $pager = $partyObj->spPager()->getPager();
                break;
        }
        $this->results = $results;
        $this->pager = $pager;
        $this->type = $type;
        $this->display('party/index.html');
    }

    public function saveParty(){
        $partyInfo = array(
            'title' => htmlspecialchars($this->spArgs('title')),
            'user_id' => $_SESSION['userInfo']['user_id'],
            'start_date' => strtotime($this->spArgs('from')),
            'end_date' => strtotime($this->spArgs('to')),
            'create_time' => time(),
            'deadline' => strtotime($this->spArgs('deadline'))
        );

        $partyObj = spClass('m_party');
        $partyObj->verifier = $partyObj->verifierAdd;
        $v = $partyObj->spVerifier($partyInfo);
        
        if(!$v){
            //pass verify
            $r = $partyObj->create($partyInfo);
            if($r){
                $partyInfo['party_id'] = $r;
                $tempMsg = array(
                    'type'=>'success',
                    'msg'=>'添加新的聚会时间统计成功！',
                    'msgNum'=>1,
                    'partyInfo'=>$partyInfo
                );
                $_SESSION['msg'] = $tempMsg;
                $this->jump(spUrl('msg','index'));
            } else {
                $tempMsg = array(
                    'type'=>'error',
                    'msg'=>'添加新的聚会时间统计失败！',
                    'msgNum'=>2
                );
                $_SESSION['msg'] = $tempMsg;
                $this->jump(spUrl('msg','index'));
            }
        } else {
            //not pass verify
            $alert = array_pop($v);
            $this->error($alert[0]);
        }
    }

    public function selectTime(){
        $partyID = (int)$this->spArgs('pid');
        if(!$partyID)$this->jump(spUrl('party','index'));

        $partyObj = spClass('m_party');
        $partyInfo = $partyObj->find(array('party_id'=>$partyID));
        //not found party
        if(!$partyInfo){
            $this->error('没有找到你想参加的聚会',spUrl('party','index'));
            return;
        }

        $period = array();
        $period = $this->caculateThePartyPeriod($partyInfo['start_date'],$partyInfo['end_date']);

        //caculate people number in every time period
        $partyTimeObj = spClass('m_user_select');
        $allThisPartyTime = $partyTimeObj->findAll(array('party_id'=>$partyID));
        $peopleCount = array();
        foreach ($allThisPartyTime as $k => $v) {
            $peopleCount[$v['date']]['t'.$v['time_period']]++;
        }

        //get my free time
        $freeTime = $partyTimeObj->findAll(array('party_id'=>$partyID,'user_id'=>$_SESSION['userInfo']['user_id']));
        foreach ($freeTime as $k => $v) {
            $myFreeTime[$v['date']][$v['time_period']] = 1;
        }

        $this->partyID = $partyID;
        $this->now = strtotime(date('Y-m-d',time()));
        $this->period = $period;
        $this->peopleCount = $peopleCount;
        $this->myFreeTime = $myFreeTime;
        $this->partyInfo = $partyInfo;
        $this->partyShowPage = 'http://'.$_SERVER['HTTP_HOST'].spUrl('party','show',array('pid'=>$partyID));
        $this->partyShowPageQrCode = spUrl('party','partyQrCode',array('pid'=>$partyID));
        $this->display('party/selectTime.html');
    }

    public function saveSel(){
        $partyID = (int)$this->spArgs('party_id');
        if(!$partyID)$this->error('数据异常',spUrl('party','index'));
        $partyObj = spClass('m_party');
        $partyInfo = $partyObj->find(array('party_id'=>$partyID));

        $sel = $this->spArgs('sel');
        $conditions = array('party_id'=>$partyID,'user_id'=>$_SESSION['userInfo']['user_id']);
        $partySel = array();

        foreach ($sel as $k => $v) {
            if($v == 1){
                $tempArr = array();
                $tempArr = explode('_', $k);
                if((int)$tempArr[0]&&(int)$tempArr[1]){
                    $arr['user_id'] = $_SESSION['userInfo']['user_id'];
                    $arr['party_id'] = $partyID;
                    $arr['date'] = $tempArr[0];
                    $arr['time_period'] = $tempArr[1];
                    array_push($partySel, $arr);
                } else {
                    $this->error('数据异常',spUrl('party','index'));
                    return;
                }
            }
        }

        $partySelObj = spClass('m_user_select');
        //$partySelObj->query("BEGIN");
        //firstly delete data
        $r1 = $partySelObj->delete($conditions);
        //then add new data
        $r2 = $partySelObj->addMore($partySel);
        if($r1 && $r2){
            $partySelObj->query("COMMIT");
            $tempMsg = array(
                'type'=>'success',
                'msg'=>'你的空闲时间保存成功！',
                'msgNum'=>3,
                'partyInfo'=>$partyInfo
            );
            $_SESSION['msg'] = $tempMsg;
            $this->jump(spUrl('msg','index'));
        } else {
            $partySelObj->query("ROLLBACK");
            $tempMsg = array(
                'type'=>'error',
                'msg'=>'你的空闲时间保存失败！',
                'msgNum'=>4
            );
            $_SESSION['msg'] = $tempMsg;
            $this->jump(spUrl('msg','index'));
        }
    }

    public function show(){
        $partyID = (int)$this->spArgs('pid');
        if(!$partyID)$this->jump(spUrl('main','index'));
        if($this->isLogin)$this->jump(spUrl('party','selectTime',array('pid'=>$partyID)));

        $partyObj = spClass('m_party');
        $partyInfo = $partyObj->find(array('party_id'=>$partyID));
        //not found party
        if(!$partyInfo){
            $this->error('没有找到你想参加的聚会',spUrl('main','index'));
            return;
        }

        $period = array();
        $period = $this->caculateThePartyPeriod($partyInfo['start_date'],$partyInfo['end_date']);

        //caculate people number in every time period
        $partyTimeObj = spClass('m_user_select');
        $allThisPartyTime = $partyTimeObj->findAll(array('party_id'=>$partyID));
        $peopleCount = array();
        foreach ($allThisPartyTime as $k => $v) {
            $peopleCount[$v['date']]['t'.$v['time_period']]++;
        }

        $this->partyID = $partyID;
        $this->now = strtotime(date('Y-m-d',time()));
        $this->period = $period;
        $this->peopleCount = $peopleCount;
        $this->partyInfo = $partyInfo;
        $this->display('party/show.html');
    }

    public function partyQrCode(){
        $partyID = (int)$this->spArgs('pid');
        $pix = (int)$this->spArgs('pix',3);
        if($partyID){
            import('phpqrcode/qrlib.php');
            header('Content-type:image/png');
            QRcode::png('http://'.$_SERVER['HTTP_HOST'].spUrl('party','show',array('pid'=>$partyID)),false,'H',$pix,5);
        } else {
            $this->jump(spUrl('main','index'));
        }
    }

    public function showDetail(){
        $results = array();
        $conditions['date'] = $this->spArgs('date');
        $conditions['time_period'] = $this->spArgs('time_period');
        $conditions['party_id'] = $this->spArgs('party_id');

        $userSelObj = spClass('m_user_select');
        $userIDs = $userSelObj->findAll($conditions,null,'user_id');

        if($userIDs){
            $idList = array();
            foreach ($userIDs as $val) {
                array_push($idList, $val['user_id']);
            }
            $ids = implode(',', $idList);
            $userObj = spClass('m_user');
            $realname = $userObj->findAll('user_id in ('.$ids.')',null,'realname');
            if($realname){
                $nameList = array();
                foreach ($realname as $v) {
                    array_push($nameList,$v['realname']);
                }
                $results['realname_str'] = implode(' , ', $nameList);
                $results['realname'] = $nameList;
                $results['code'] = 1;
            } else {
                $results['code'] = 0;
            }
            header('Content-type:text/html;charset=utf-8');
            echo json_encode($results);
            return;
        } else {
            $results['code'] = 0;
            header('Content-type:text/html;charset=utf-8');
            echo json_encode($results);
            return;
        }
    }

    private function caculateThePartyPeriod($firstDayTimestamp,$lastDayTimestamp){
        if(!$firstDayTimestamp || !$lastDayTimestamp)return false;
        $period = array();
        $i = 0;

        $tempTime = $firstDayTimestamp;
        while($tempTime<=$lastDayTimestamp){
            $tempDate = array(
                'title'=>date('m月d日',$tempTime).' 周'.$this->week[date('w',$tempTime)],
                'timestamp'=>$tempTime
            );
            array_push($period, $tempDate);
            $tempTime += 24*60*60;
            $i++;
        }
        return $period;
    }
}