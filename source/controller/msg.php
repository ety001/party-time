<?php
class msg extends authParent
{
    public function index(){
        /*
         1--add party success
         2--add party failed
         3--add party select success
         4--add party select failed
         */
        //get the msg from SESSION
        $msgSession = $_SESSION['msg'];
        unset($_SESSION['msg']);
        $msgNum = $msgSession['msgNum'];
        if($msgNum==0)$this->error('error msgNum');

        switch ($msgNum) {
            case 1:
                //come from adding party success
                $partyInfo = $msgSession['partyInfo'];
                $partyID = $partyInfo['party_id'];
                $this->desc = '您的好友 '.$_SESSION['userInfo']['realname'].' 发起了一个聚会邀请您参加，请您点击链接以选择您的空闲时间，聚会的内容如下：'.$partyInfo['title'];
                $this->partyURL = 'http://'.$_SERVER['HTTP_HOST'].spUrl('party','show',array('pid'=>$partyID));
                $this->partyURLQrCode = spUrl('party','partyQrCode',array('pid'=>$partyID));
                break;
            case 3:
                //come from adding party select success
                $partyInfo = $msgSession['partyInfo'];
                $partyID = $partyInfo['party_id'];
                $this->partyInfo = $partyInfo;
                $this->desc = '您参与的一个聚会投票已经截止，请您点击下面的链接来查看结果，聚会的内容如下：'.$partyInfo['title'];
                $this->partyURL = 'http://'.$_SERVER['HTTP_HOST'].spUrl('party','show',array('pid'=>$partyID));
                break;
        }

        //get the msg type
        $msgType = $msgSession['type'];
        //init 2 params
        $msg = '';
        $msgClassArr = array(
            'success' => 'alert-success',
            'error' => 'alert-error'
        );

        if($msgType){
            $msgType = ($msgType=='success')?'success':'error';
            $msg = strip_tags($msgSession['msg']);
        }else{
            $msgType = 'error';
            $msg = '未知错误';
        }

        $this->msgNum = $msgNum;
        $this->msg = $msg;
        $this->msgType = $msgType;
        $this->msgClass = $msgClassArr[$this->msgType];
        $this->display('message.html');
    }
}