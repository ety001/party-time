<?php
class cron extends spController
{
    public function index(){
        $this->jump('main','index');
    }

    public function closeOvertimeParty(){
        $partyObj = spClass('m_party');
        $conditions = 'deadline <= '.time();
        $partyObj->updateField($conditions,'status',0);
    }
}