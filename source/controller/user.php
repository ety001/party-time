<?php
class user extends authParent
{
    public function index(){
        $this->jump(spUrl('main','index'));
    }
    
    public function register(){
        if($this->isLogin){
            $this->jump(spUrl('party','index'));
            return;
        }
        //lastPage is Controller|Action|Param|Val
        $lastPage = $this->spArgs('lastPage');
        if(isset($lastPage)){
            $_SESSION['lastPage'] = $lastPage;
        }
        $this->display('user/register.html');
    }

    public function addSave(){
        /*TODO
        xss
        */
        //get the page url where user comes from
        $lastPage = $_SESSION['lastPage'];
        unset($_SESSION['lastPage']);
        if(!$lastPage){
            $lastPageURI = spUrl('user','signin');
        } else {
            $arr = explode('|', $lastPage);
            if(count($arr)==2){
                $lastPageURI = spUrl($arr[0],$arr[1]);
            } else if(count($arr)==4) {
                $lastPageURI = spUrl($arr[0],$arr[1],array($arr[2]=>$arr[3]));
            }
        }
        //get the user's register info
        $userInfo = array();
        $userInfo = $this->spArgs();
        unset($userInfo['c']);
        unset($userInfo['a']);
        
        $userObj = spClass('m_user');
        $userObj->verifier = $userObj->verifierReg;
        $v = $userObj->spVerifier($userInfo);

        if(!$v){
            //pass verify
            $userInfo['password'] = md5($userInfo['password']);
            $r = $userObj->create($userInfo);
            if($r){
                $this->success('注册成功',$lastPageURI);
            } else {
                $this->error('注册失败',spUrl('user','register'));
            }
        } else {
            //not pass verify
            $alert = array_pop($v);
            $this->error($alert[0],spUrl('user','register'));
        }
    }

    public function signin(){
        if($this->isLogin){
            $this->jump(spUrl('party','index'));
            return;
        }
        //lastPage is Controller|Action|Param|Val
        $lastPage = $this->spArgs('lastPage');
        if(isset($lastPage)){
            $_SESSION['lastPage'] = $lastPage;
        }
        $this->display('user/signin.html');
    }

    public function signAuth(){
        //get the page url where user comes from
        $lastPage = $_SESSION['lastPage'];
        unset($_SESSION['lastPage']);
        if(!$lastPage){
            $lastPageURI = spUrl('party','index');
        } else {
            $arr = explode('|', $lastPage);
            if(count($arr)==2){
                $lastPageURI = spUrl($arr[0],$arr[1]);
            } else if(count($arr)==4) {
                $lastPageURI = spUrl($arr[0],$arr[1],array($arr[2]=>$arr[3]));
            }
        }

        $userInfo = $this->spArgs();
        $confident = array('email'=>$userInfo['email']);

        $userObj = spClass('m_user');
        $userObj->verifier = $userObj->verifierSignin;

        $v = $userObj->spVerifier($userInfo);
        if(!$v){
            //pass verify
            $user = $userObj->find($confident);
            if(!$user){
                $this->error('帐号不存在',spUrl('user','register'));
                return;
            }
            if($user['password'] == md5($userInfo['password'])){
                spClass('spAcl')->set('GBUSER');
                unset($user['password']);
                $_SESSION['userInfo'] = $user;
                $this->success('登录成功',$lastPageURI);
                return;
            } else {
                $this->error('密码错误',spUrl('user','signin'));
                return;
            }
        } else {
            //not pass verify
            $alert = array_pop($v);
            $this->error($alert[0],spUrl('user','signin'));
        }
    }

    public function signout(){
        spClass('spAcl')->set('');
        unset($_SESSION['userInfo']);
        unset($_SESSION);
        $this->success('退出成功',spUrl('main','index'));
    }
}